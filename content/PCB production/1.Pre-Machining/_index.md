Pre-Machining process

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->

First of all we had the design of the board ready on kicad, so I needed to install kicad to  open the design on it. So, I installed kicad from: https://www.kicad.org/download/windows/ then with a straight forward next next process it was installed successfully. but it need 1.4GB whiile downloading so make sure to have a good internet connection :D

![img](/media/a1.PNG)

Then opened the design files and looked like this: 

![img](/media/a2.PNG)

I need a GERBER file extension from this design. so from file > Plot > click plot button to generate .gbr files and also " generate drill file " button to export drill files too. 
 
![img](/media/a3.PNG)

After GERBER files were ready, I needed to view them. Gerbv is a suitable software to do so, first download it from here: https://sourceforge.net/projects/gerbv/ . It will open right away after installation. Then, open it and file > open layer > choose all the GERBER files we exported, then choose layer tab. It should be like this photo:

![img](/media/a4.PNG)

Uncheck all layer except outline and top layer then from file > export as pdf

![img](/media/a5.PNG)

Then uncheck top and check drills layer then export a new pdf

![img](/media/a6.PNG)

Now we are done with Gerbv. To be able to edit the PDFs we generated I needed another software. GIMP is a really good one for editing pictures. download link: https://www.gimp.org/downloads/ . After that, open Gimp then file > open > choose traces file " top.pdf " while you open the file a new window will pop-up, don't forget to set resolution to 1000

![img](/media/a7.png)

After you open the pdf > right click then from select click on select "by color" and click on one trace then it will automatically select all the traces

![img](/media/a8.png)

Then click on bucket fill and fill them with black color

![img](/media/a9.png)

Right click and from slect choose none then color the outline with white color like the rest of the background.

![img](/media/a10.PNG)

Then from color choose invert this will invert all colors making the traces white and the background black

![img](/media/a11.png)

Now that our pdf is ready from file > export as top.png

![img](/media/a12.png)

Open the drills pdf then repeat the last process making the drills and outline color - black then export outline.png

![img](/media/a13.png)